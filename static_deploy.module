<?php

/**
 * @file
 *
 * Module file for the Static Deploy module.
 *
 * Contains hook implementations and general purpose functions.
 */

/**
 * Identifier for log entries.
 */
define('STATIC_DEPLOY_WD', 'static');

/**
 * Implements hook_menu().
 */
function static_deploy_menu() {
  $items['admin/config/system/static/deploy'] = array(
    'title' => 'Deployment settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('static_deploy_admin_settings_form'),
    'access arguments' => array('administer static site'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'static_deploy.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function static_deploy_form_static_admin_status_alter(&$form, &$form_state) {
  $result = db_select('static', 's')
    ->fields('s')
    ->condition('status', 200)
    ->execute();
  $count = $result->rowCount();
  $form['actions']['deploy'] = array(
    '#type' => 'submit',
    '#value' => t('Deploy'),
    '#submit' => array('static_deploy_form_static_admin_status_submit'),
    '#disabled' => $count == 0,
  );
}

/**
 * Submit handler for the static admin status form.
 */
function static_deploy_form_static_admin_status_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $form_state['redirect'] = 'admin/config/system/static';

  switch ($values['op']) {
    case t('Deploy'):
      module_load_include('inc', 'static_deploy', 'static_deploy.batch');
      static_deploy_batch_deployment_create();
      break;
  }
}

/**
 * Clean file list from base directory.
 *
 * @param array $list
 *  A list of filenames that might include a base directory.
 * @param string $basedir
 *  The string to remove from each entry in the file list.
 */
function static_deploy_clean_file_list(&$list, $basedir) {
  foreach ($list as $key => $file_name) {
    $list[$key] = substr($file_name, strlen(rtrim($basedir, '/')) + 1);
  }
}

/**
 * Retrieve a list of the local files that are ready for deployment.
 *
 * @param string $dir
 *  The directory to read the files from.
 * @return array
 *  An array of file paths, relative to the given base directory.
 */
function static_deploy_get_local_files($dir) {
  $files = array();
  foreach (scandir($dir) as $file) {
    if ($file == '.' || $file == '..' || static_deploy_is_excluded($file)) {
      continue;
    }
    if (is_dir($dir . '/' . $file)) {
      $files = array_merge($files, static_deploy_get_local_files($dir . '/' . $file));
    }
    else {
      $files[] = $dir . '/' . $file;
    }
  }
  return $files;
}

/**
 * Check if the given string matches one of the exclude patterns.
 *
 * @param string $string
 *  The string to match for.
 * @return boolean
 *  TRUE if the given string matches an existing exclude pattern, FALSE
 *  otherwhise.
 */
function static_deploy_is_excluded($string) {
  $settings = static_deploy_get_settings();
  if (empty($settings['exclude'])) {
    return FALSE;
  }
  $excluded = explode("\n", $settings['exclude']);
  if (!count($excluded)) {
    return FALSE;
  }
  foreach ($excluded as $pattern) {
    $pattern = trim($pattern);
    if (fnmatch($pattern, $string) !== FALSE) {
      return TRUE;
    }
  }
}

/**
 * Retrieve all enabled deployment methods.
 *
 * @return array
 *  A list of the available deployment methods, with the methods machine name
 *  as key and the label as value. Suitable to use in select fields.
 */
function static_deploy_get_enabled_deployment_methods() {
  $possible_methods = array(
    'ftp' => t('FTP'),
    'sftp' => t('SFTP (SSH)'),
    'copy' => t('Copy (same server)'),
  );
  $methods = array();
  foreach ($possible_methods as $method => $label) {
    if (static_deploy_method_available($method)) {
      $methods[$method] = $label;
    }
    else {
      $methods_unavailable[$method] = $label;
    }
  }
  return $methods;
}

/**
 * Check whether the given deployment method is available.
 *
 * @param string $method
 *  The machine name of the method to check for.
 * @return boolean
 *  TRUE if the method can be used, FALSE otherwhise.
 */
function static_deploy_method_available($method) {
  switch ($method) {
    case 'ftp':
      // Check that ftp functions exists.
      return function_exists('ftp_connect');
      break;
    case 'sftp':
      // Check for stream wrappers.
      return extension_loaded('ssh2');
      break;
    case 'copy':
      // Always there.
      return TRUE;
      break;
  }
}

/**
 * Retrieve the settings for publishing.
 *
 * @return array
 *  Settings array, see the default definition below for the structure.
 */
function static_deploy_get_settings() {
  return variable_get('static_deploy_settings', array(
    'method' => 'ftp',
    'ftp' => array(
      'server' => '',
      'port' => '',
      'user' => '',
      'pass' => '',
      'timeout' => '',
      'dir' => '',
    ),
    'sftp' => array(
      'server' => '',
      'port' => '',
      'user' => '',
      'pass' => '',
      'dir' => '',
    ),
    'copy' => array(
      'dir' => '',
    ),
    'exclude' => '',
  ));
}
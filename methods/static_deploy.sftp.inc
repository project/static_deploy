<?php

/**
 * @file
 *
 * Deployment method for the Static Deploy module.
 *
 * Contains interfacing logic for deployments using SFTP.
 */

/**
 * Login to the SFTP server
 *
 * @return resource
 */
function static_deploy_sftp_login() {
  $settings = static_deploy_get_settings();
  $connection = ssh2_connect($settings['sftp']['server'], $settings['sftp']['port']);
  ssh2_auth_password($connection, $settings['sftp']['user'], $settings['sftp']['pass']);
  $sftp = ssh2_sftp($connection);
  if (!$sftp) {
    watchdog(STATIC_DEPLOY_WD, 'Fatal Error: Connection to SFTP server failed.', array(), WATCHDOG_ERROR);
    return FALSE;
  }
  return $sftp;
}

/**
 * Recursively retrieve the current filelist on the sftp server.
 *
 * @param resource $sftp
 * @param string $dir
 * @return array
 */
function static_deploy_sftp_get_file_list($sftp, $dir) {
  $file_list = array();
  $dir = rtrim($dir, '/');
  $dir_list = scandir("ssh2.sftp://$sftp/$dir");
  if (!$dir_list) {
    return array();
  }
  foreach ($dir_list as $entry) {
    // Some _FTP_ servers return complete paths, others only the filenames,
    // maybe true for SFTP too, so just in case ...
    $file_name = (strpos($entry, $dir) === 0) ? substr($entry, strlen(rtrim($dir, '/')) + 1) : $entry;
    if ($file_name == '.' || $file_name == '..' || static_deploy_is_excluded($file_name)) {
      // Skip certain paths.
      continue;
    }
    if (static_deploy_sftp_is_dir($sftp, $dir . '/' . $file_name)) {
      // Recurse into the directory.
      $file_list = array_merge($file_list, static_deploy_sftp_get_file_list($sftp, $dir . '/' . $file_name));
    }
    else {
      // Add file to the list.
      $file_list[] = $dir . '/' . $file_name;
    }
  }
  return $file_list;
}

/**
 * Check if the given string is a directory on the sftp server.
 *
 * @param resource $sftp
 * @param string $dir
 * @return boolean
 */
function static_deploy_sftp_is_dir($sftp, $dir) {
  static $settings;
  if (!$settings) {
    $settings = static_deploy_get_settings();
  }
  return is_dir("ssh2.sftp://$sftp/$dir");
}

/**
 * Upload a local file to a remote location.
 *
 * @param resource $sftp
 * @param string $local_file
 * @param string $remote_file
 */
function static_deploy_sftp_upload_file($sftp, $local_file, $remote_file) {
  $f = @fopen("ssh2.sftp://$sftp/$remote_file", 'w');
  if (!$f) {
    ssh2_sftp_mkdir($sftp, dirname($remote_file), 0777, TRUE);
    $f = fopen("ssh2.sftp://$sftp/$remote_file", 'w');
  }
  if (!$f) {
    watchdog(STATIC_DEPLOY_WD, 'Failed to upload ' . $local_file . ' to ' . $remote_file, array(), WATCHDOG_ERROR);
    return;
  }
  fwrite($f, file_get_contents($local_file));
  fclose($f);
}
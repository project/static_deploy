<?php

/**
 * @file
 *
 * Deployment method for the Static Deploy module.
 *
 * Contains interfacing logic for deployments using FTP.
 */

/**
 * Login to the FTP server.
 *
 * @return ressource
 */
function static_deploy_ftp_login() {
  $settings = static_deploy_get_settings();
  $conn_id = ftp_connect($settings['ftp']['server'], $settings['ftp']['port'], $settings['ftp']['timeout']);
  $login_result = @ftp_login($conn_id, $settings['ftp']['user'], $settings['ftp']['pass']);
  ftp_pasv($conn_id, TRUE);
  if (!$login_result) {
    watchdog(STATIC_DEPLOY_WD, 'Fatal Error: Connection to FTP server failed.', array(), WATCHDOG_ERROR);
    return FALSE;
  }
  return $conn_id;
}

/**
 * Recursively retrieve the current filelist on the ftp server.
 *
 * @param resource $conn_id
 * @param string $dir
 * @return array
 */
function static_deploy_ftp_get_file_list($conn_id, $dir) {
  $file_list = array();
  $dir = rtrim($dir, '/');
  $dir_list = ftp_nlist($conn_id, $dir);
  if (!$dir_list) {
    return array();
  }
  foreach ($dir_list as $entry) {
    // Some FTP servers return complete paths, others only the filenames.
    $file_name = (strpos($entry, $dir) === 0) ? substr($entry, strlen(rtrim($dir, '/')) + 1) : $entry;
    if ($file_name == '.' || $file_name == '..' || static_deploy_is_excluded($file_name)) {
      // Skip certain paths.
      continue;
    }
    if (static_deploy_ftp_is_dir($conn_id, $dir . '/' . $file_name)) {
      // Recurse into the directory.
      $file_list = array_merge($file_list, static_deploy_ftp_get_file_list($conn_id, $dir . '/' . $file_name));
    }
    else {
      // Add file to the list.
      $file_list[] = $dir . '/' . $file_name;
    }
  }
  return $file_list;
}

/**
 * Check if the given string is a directory on the ftp server.
 *
 * @param resource $conn_id
 * @param string $dir
 * @return boolean
 */
function static_deploy_ftp_is_dir($conn_id, $dir) {
  static $settings;
  if (!$settings) {
    $settings = static_deploy_get_settings();
  }
  $user = $settings['ftp']['user'];
  $pass = $settings['ftp']['pass'];
  $server = $settings['ftp']['server'];
  return is_dir("ftp://$user:$pass@$server/" . $dir);
}

/**
 * Create a directory.
 *
 * @param resource $conn_id
 * @param string $directory
 */
function static_deploy_ftp_mkdir($conn_id, $directory) {
  if (static_deploy_ftp_is_dir($conn_id, $directory) || @ftp_mkdir($conn_id, $directory)) {
    return TRUE;
  }
  if (!static_deploy_ftp_mkdir($conn_id, dirname($directory))) {
    return FALSE;
  }
  return ftp_mkdir($conn_id, $directory);
}

/**
 * Upload a local file to a remote location.
 *
 * @param resource $conn_id
 * @param string $local_file
 * @param string $remote_file
 * @return void
 */
function static_deploy_ftp_upload_file($conn_id, $local_file, $remote_file) {
  if (!static_deploy_ftp_is_dir($conn_id, dirname($remote_file))) {
    static_deploy_ftp_mkdir($conn_id, dirname($remote_file));
  }
  if (!ftp_put($conn_id, $remote_file, $local_file, FTP_BINARY)) {
    watchdog(STATIC_DEPLOY_WD, 'Failed to upload ' . $local_file . ' to ' . $remote_file, array(), WATCHDOG_ERROR);
  }
}
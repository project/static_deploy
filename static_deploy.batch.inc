<?php

/**
 * @file
 *
 * Include file for the Static Deploy module.
 *
 * Contains batch related functions.
 */

/**
 * Creates and sets a deployment batch.
 */
function static_deploy_batch_deployment_create() {
  $settings = static_deploy_get_settings();
  switch ($settings['method']) {
    case 'ftp':
      $operations = array(
        array('static_deploy_batch_deployment_ftp_prepare', array()),
        array('static_deploy_batch_deployment_ftp_process', array()),
      );
      break;
    case 'sftp':
      $operations = array(
        array('static_deploy_batch_deployment_sftp_prepare', array()),
        array('static_deploy_batch_deployment_sftp_process', array()),
      );
      break;
    case 'copy':
      $operations = array(
        array('static_deploy_batch_deployment_copy_callback', array()),
      );
      break;
  }
  $batch = array(
    'title' => t('Generating pages'),
    'operations' => $operations,
    'progress_message' => t('Completed about @percentage% of the deployment operation.'),
    'finished' => 'static_deploy_batch_deployment_finished',
    'file' => drupal_get_path('module', 'static_deploy') . '/static_deploy.batch.inc',
  );
  batch_set($batch);
  return TRUE;
}

/**
 * Prepare deployment over FTP.
 *
 * @param string $context
 */
function static_deploy_batch_deployment_ftp_prepare(&$context) {
  $settings = static_deploy_get_settings();
  watchdog(STATIC_DEPLOY_WD, 'Starting deployment to ' . $settings['ftp']['server']);

  // Connect to the FTP server.
  module_load_include('inc', 'static_deploy', 'methods/static_deploy.ftp');
  $conn_id = static_deploy_ftp_login();
  if (!$conn_id) {
    $context['finished'] = TRUE;
    return;
  }

  // Retrieve a comprehensive list of files in the given directory.
  $remote_list = static_deploy_ftp_get_file_list($conn_id, $settings['ftp']['dir']);
  static_deploy_clean_file_list($remote_list, $settings['ftp']['dir']);
  $context['results']['active']['file_list_remote'] = $remote_list;

  ftp_close($conn_id);
}

/**
 * Process deployment over FTP.
 *
 * @param string $context
 */
function static_deploy_batch_deployment_ftp_process(&$context) {
  $settings = static_deploy_get_settings();
  $basedir = rtrim($settings['ftp']['dir'], '/');
  $static_dir = static_get_normal_cache_dir();

  module_load_include('inc', 'static_deploy', 'methods/static_deploy.ftp');
  $conn_id = static_deploy_ftp_login();
  if (!$conn_id) {
    $context['finished'] = TRUE;
    return;
  }

  $file_list_remote = $context['results']['active']['file_list_remote'];

  $file_list_local = static_deploy_get_local_files($static_dir);
  static_deploy_clean_file_list($file_list_local, $static_dir);

  // Find deleted files.
  $removed = array_diff($file_list_remote, $file_list_local);
  if (count($removed)) {
    watchdog(STATIC_DEPLOY_WD, 'Found ' . count($removed) . ' old items to remove from ' . $settings['ftp']['server']);
    foreach ($removed as $file) {
      ftp_delete($conn_id, $basedir . '/' . $file);
    }
  }

  // Now push all the other local files to the remote.
  foreach ($file_list_local as $file) {
    static_deploy_ftp_upload_file($conn_id, $static_dir . '/' . $file, $basedir . '/' . $file);
  }

  ftp_close($conn_id);
  watchdog(STATIC_DEPLOY_WD, 'Finished deployment to ' . $settings['ftp']['server']);
}

/**
 * Prepare deployment over SFTP.
 *
 * @param string $context
 */
function static_deploy_batch_deployment_sftp_prepare(&$context) {
  $settings = static_deploy_get_settings();
  watchdog(STATIC_DEPLOY_WD, 'Starting deployment to ' . $settings['sftp']['server']);

  // Connect to the SFTP server.
  module_load_include('inc', 'static_deploy', 'methods/static_deploy.sftp');
  $sftp = static_deploy_sftp_login();
  if (!$sftp) {
    $context['finished'] = TRUE;
    return;
  }
  // Retrieve a comprehensive list of files in the given directory.
  $remote_list = static_deploy_sftp_get_file_list($sftp, $settings['sftp']['dir']);
  static_deploy_clean_file_list($remote_list, $settings['sftp']['dir']);
  $context['results']['active']['file_list_remote'] = $remote_list;
}

/**
 * Process deployment over SFTP.
 *
 * @param string $context
 */
function static_deploy_batch_deployment_sftp_process(&$context) {
  $settings = static_deploy_get_settings();
  $basedir = rtrim($settings['sftp']['dir'], '/');
  $static_dir = static_get_normal_cache_dir();

  module_load_include('inc', 'static_deploy', 'methods/static_deploy.sftp');
  $sftp = static_deploy_sftp_login();
  if (!$sftp) {
    $context['finished'] = TRUE;
    return;
  }

  $file_list_remote = $context['results']['active']['file_list_remote'];

  $file_list_local = static_deploy_get_local_files($static_dir);
  static_deploy_clean_file_list($file_list_local, $static_dir);

  // Find deleted files.
  $removed = array_diff($file_list_remote, $file_list_local);
  if (count($removed)) {
    watchdog(STATIC_DEPLOY_WD, 'Found ' . count($removed) . ' old items to remove from ' . $settings['sftp']['server']);
    foreach ($removed as $file) {
      ssh2_sftp_unlink($sftp, $basedir . '/' . $file);
    }
  }

  // Now push all the other local files to the remote.
  foreach ($file_list_local as $file) {
    static_deploy_sftp_upload_file($sftp, $static_dir . '/' . $file, $basedir . '/' . $file);
  }

  watchdog(STATIC_DEPLOY_WD, 'Finished deployment to ' . $settings['sftp']['server']);
}

/**
 * Process deployment by local copy.
 *
 * @param string $context
 */
function static_deploy_batch_deployment_copy_callback($context) {
  $settings = static_deploy_get_settings();
  $basedir = rtrim($settings['copy']['dir'], '/');
  $static_dir = static_get_normal_cache_dir();

  $file_list_remote = static_deploy_get_local_files($basedir);
  static_deploy_clean_file_list($file_list_remote, $basedir);

  $file_list_local = static_deploy_get_local_files($static_dir);
  static_deploy_clean_file_list($file_list_local, $static_dir);

  // Find deleted files.
  $removed = array_diff($file_list_remote, $file_list_local);
  if (count($removed)) {
    foreach ($removed as $file) {
      unlink($basedir . '/' . $file);
    }
  }

  // Now push all the other local files to the remote.
  foreach ($file_list_local as $file) {
    $local_file = $static_dir . '/' . $file;
    $remote_file = $basedir . '/' . $file;
    // Check for directory
    if (!is_dir(dirname($remote_file))) {
      mkdir(dirname($remote_file), 0777, true);
    }
    copy($local_file, $remote_file);
  }
}

function static_deploy_batch_deployment_finished() {

}
<?php

/**
 * Implements hook_form().
 */
function static_deploy_admin_settings_form($form, &$form_state) {

  $settings = static_deploy_get_settings();

  $methods = array();
  $possible_methods = array(
    'ftp' => t('FTP'),
    'sftp' => t('SFTP (SSH)'),
    'copy' => t('Copy (same server)'),
  );
  foreach ($possible_methods as $method => $label) {
    if (static_deploy_method_available($method)) {
      $methods[$method] = $label;
    }
  }

  $form['method'] = array(
    '#type' => 'radios',
    '#title' => t('Publishing method'),
    '#options' => $methods,
    '#default_value' => $settings['method'],
  );

  $form['ftp'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('FTP'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => array(
        ':input[name="method"]' => array('!value' => 'ftp'),
      ),
    ),
  );
  $form['ftp']['server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server'),
    '#default_value' => $settings['ftp']['server'],
    '#rules' => array(
      'ipv4',
    ),
  );
  $form['ftp']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => $settings['ftp']['port'],
  );
  $form['ftp']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => $settings['ftp']['user'],
  );
  $form['ftp']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => $settings['ftp']['pass'],
  );
  $form['ftp']['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout'),
    '#default_value' => $settings['ftp']['timeout'],
  );
  $form['ftp']['dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory'),
    '#default_value' => $settings['ftp']['dir'],
  );

  $form['sftp'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('SFTP (SSH)'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => array(
        ':input[name="method"]' => array('!value' => 'sftp'),
      ),
    ),
  );
  $form['sftp']['server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server'),
    '#default_value' => $settings['sftp']['server'],
    '#rules' => array(
      'ipv4',
    ),
  );
  $form['sftp']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => $settings['sftp']['port'],
  );
  $form['sftp']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => $settings['sftp']['user'],
  );
  $form['sftp']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => $settings['sftp']['pass'],
  );
  $form['sftp']['dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory'),
    '#default_value' => $settings['sftp']['dir'],
  );

  $form['copy'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Copy (same server)'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => array(
        ':input[name="method"]' => array('!value' => 'copy'),
      ),
    ),
  );
  $form['copy']['dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory'),
    '#default_value' => $settings['copy']['dir'],
  );

  $form['exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude patterns'),
    '#default_value' => $settings['exclude'],
    '#description' => t('Enter one pattern per line. A pattern can be anything understandable by shell copy command <em>cp</em>, e.g.: <em>.htaccess</em>, <em>files/*</em>'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the settings form.
 */
function static_deploy_admin_settings_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  variable_set('static_deploy_settings', array(
    'method' => $values['method'],
    'ftp' => $values['ftp'],
    'sftp' => $values['sftp'],
    'copy' => $values['copy'],
    'exclude' => $values['exclude'],
  ));
  drupal_set_message('The changes have been saved.');
}